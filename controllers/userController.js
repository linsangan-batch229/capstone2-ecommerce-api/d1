//          [MODELS]
const User = require('../models/User.js');
const Product = require('../models/Product.js');
const Order = require('../models/Order');
  
const bcrypt = require('bcrypt');   //[PASSWORD ENCYRPT]
const auth = require('../auth.js');


//          [FUNCTION - REGISTER]
/*
STEPS
1. Create a new User object using the mongoose model of 'Users'
    and the information from the reqyest body
*/

module.exports.registerUser =  (reqBody) => {

    //check  first if user_email is already exists in DB
    // return boolean
    return   User.find({user_email : reqBody.user_email})
        .then(result => {
            if (result.length > 0) {
                return false;
            } else {
                //call the function if < 0
                return createUser();
            }
        });
 
    //function that create the user if not  existing
    function createUser(){
//     //get data from reqBody and stored it only, not yet saved
        let newUser = new User({
            user_email : reqBody.user_email,
            user_password : bcrypt.hashSync(reqBody.user_password, 10)
        });

        //save the hold data from newUser
        return  newUser.save().then((user, error) => {
            if (user) {
                return true
            } else {
                return false;
            }
        })
    }
}



//          [LOGIN TO SHOW AN AUTH]
/*
STEPS:
    1. get the users input and check in DB if its registered
*/
module.exports.login = (reqBody) => {
    return User.findOne({user_email : reqBody.user_email})
        .then(result => {
            if(result == null){
                return false;
            } else {

                 //compare the password from DB vs entered password
                const isPasswordCorrect = bcrypt.compareSync(reqBody.user_password, result.user_password);

                if (isPasswordCorrect){
                    return {Token : auth.createAccessToken(result)};
                } else {
                    return false;
                }
            }
        
        })
}


//              [RETRIEVE USER DETAILS]
module.exports.getUserDetails = (reqParams) => {

    //get the req.params and use it with findById on user collection
    return User.findById(reqParams.userId)
        .then((result, error) => {
            if (result) {
                result.user_password = "**********";
                return true;
            } else {
                return false;
            }
            
        })
}



//                [RETRIEVE USER DETAILS USING TOKEN]
module.exports.getProfile = (info) => {
    console.log("info --" + info)

    return User.findById(info.userId)
        .then((result, error) => {
            console.log("result --" + result);
            return result;
        });

}



//              [SET A USER TO ADMIN]
module.exports.setAdmin = (reqParams, isAdmin, reqBody) => {


    // granted or remove admin return message
    function toAdmin(){
        if (reqBody.user_isAdmin) {
            return true;
        } else {
            return false;
        }

    }
    
    //1. get the id from body
    //2. use the User.findByIdAndUpdate
    if(isAdmin && reqBody !== 'undefined') {

        let setToAdmin = {
            user_isAdmin : reqBody.user_isAdmin
        }

        return User.findByIdAndUpdate(reqParams.userId, setToAdmin)
                    .then((result, error) => {
                        if(error) {
                            return false;
                        } else {
                            return toAdmin();
                        }
                    })

    } else {
        return Promise.reject('Failed: Not authorized to proceed or the request body is missing')
    }
}
