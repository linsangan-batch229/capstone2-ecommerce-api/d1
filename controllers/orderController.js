//          [MODEL]
const User = require('../models/User.js');
const Product = require('../models/Product.js');
const Order = require('../models/Order');

const auth = require('../auth.js');
const { findOneAndUpdate } = require('../models/Product.js');

//          [CREATE ORDER (ALL USERS)]
/* Steps
1. get the data from reqBody
2. get the req.params to track the ID
3. create a new variable to store the reqBody and req.params
4. save the new variable to the Order DB together with data from body and Id from req.params
 */

module.exports.createOrder = async (reqBody) => {

    console.log('wow');
    //reconstruct the data from outside for easy accessing it inside
    //get the data from reqBody and save it first to a variable
    // hold only not yet saved
    let ordered = {
        userId : reqBody.order_userId,
        productId : reqBody.order_productId,
        productQuantity: reqBody.order_productQuantity
    }


    //test if the user is an admin, return T/F
    let isUserAdmin = await User.findById(ordered.userId)
                .then((result, error) => {
                    if(result.user_isAdmin) {
                        return true;
                    } else {
                        return false;
                    }
                });

    //test if the user has already existing orders
    // not yet checkout
    let isUserIdExist = await Order.find({order_userId: ordered.userId}).then((result, error) => {
            if(error) {
                return error
            } else if(result.length > 0) {
                return true
            } else {
                return false
            }
    });
                

    // finalize the order
    // prepare the data
    function prepareOrder(result) {
        //prepare the ordered data before saving
        //not yet saved

        //check if user has existing order
        //if yes, just insert the newly ordered item
        //if not, create new product
        //use '!'
        if(!isUserIdExist){

            //i
            let newOrder =  new Order({
                order_userId : ordered.userId,
                order_products : [{
                    order_productId: ordered.productId,    
                    order_productName : result.product_name,
                    order_productDescription : result.product_description,
                    order_productPrice : result.product_price,
                    order_productQuantity : ordered.productQuantity,
                    order_productTotalPrice : result.product_price * ordered.productQuantity
                }]
            })

          


            //saving the newOrder to DB
            return  newOrder.save().then((result, error) => {
                if(result) {
                    return result;
                } else {
                    return false;
                    
                }
            })  


        } else {
       
            //if the user has existing order
            //just add this newly created order
            //in order DB, 1 userID with many order
            let addToExistingUserId = {
                order_productId: ordered.productId,    
                order_productName : result.product_name,
                order_productDescription : result.product_description,
                order_productPrice : result.product_price,
                order_productQuantity : ordered.productQuantity,
                order_productTotalPrice : result.product_price * ordered.productQuantity
            }

            return Order.findOneAndUpdate(
                {order_userId : ordered.userId},
                {$push : {order_products: addToExistingUserId}},
                {returnNewDocument: true}
            ).then((res,err) => {
                if(res) {
                    return res;
                } else {
                    return false;
                }
            })
        }





    }



    //check the products_quantity vs order_productsQuantity
    // if not, then proceed
      function finalizeOrder() {
        return Product.findById(ordered.productId)
        .then((result, error) => {
            if (error){
                return error
            
            //if products_quantity < order_productsQuantity, throw msg
            } else if (result.product_quantity < ordered.productQuantity){
                // return `Failed to create an order.\nAvaiable stocks for this product is only : ${result.product_quantity}`
                return `${result.product_quantity}`
            } else {
            //proceed to create the order
                return   prepareOrder(result)
            }
        })       
    }


    //if the user is admin, throw an error, cannot proceed
    //else, proceed to create the order
    if (!isUserAdmin) {
        //call the function prepareorder()
        //finalize the information and save the info after
        return await finalizeOrder();
         
    } else {
        return Promise.reject(false);
    }
     
}   //end of module.export.createOrder





//              [GET ALL ORDERS]
module.exports.getAllOrders = (isAdmin) => {

    //if admin
    if (isAdmin) {

        return Order.find({}).then((result, error) => {
            if(result) {
                return true;
            } else {
                return false;
            }
        })

    } else {
        return Promise.reject('Failed: Not authorized to proceed.');
    }
}



//              [GET USER'S ORDER]
module.exports.getOrder = (reqParams) => {

    return Order.find({order_userId : reqParams.userId})
                .then(result => {
                    if (result.length > 0) {
                        return true;
                    } else {
                        return false;
                    }
                });
}