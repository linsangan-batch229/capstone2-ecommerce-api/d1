const mongoose = require('mongoose');

//          [USERS SCHEMA]
const userSchema = new mongoose.Schema({
    user_email : {
        type : String,
        required: [true, "Email is required"]
    },

    user_password : {
        type: String,
        required: [true, "Password is required"]
    },

    user_isAdmin : {
        type : Boolean,
        default: false
    }
})


//          [USER MODEL]
module.exports = mongoose.model("User", userSchema);