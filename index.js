//          [SERVER PREPARTION]
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const app = express();

//          [ROUTES]
const userRoutes = require('./routes/user.js');
const productRoutes = require('./routes/product.js');
const orderRoutes = require('./routes/order.js');
const addToCartRoutes = require('./routes/addToCart.js');



//          [MONGODB CONNECTION]
//          [API LINK]
mongoose.connect('mongodb+srv://admin:admin1234@cluster0.2bdmdsh.mongodb.net/ecommerce-DB?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})


//          [MONGODB CONNECTION .LISTEN / SUCCESS OR FAIL]
//          [MONGODB CONNECTION NOTIFICATION]
let db = mongoose.connection;

//if not running
db.on("error", console.error.bind(console, "MongoDB: connection error"));

//if running
db.once("open", () => console.log("MongoDB: Connected to the cloud database"));


//          [MIDDLEWARES]
app.use(express.json());
app.use(express.urlencoded( {extended: true}));
app.use(cors());

//          [DEFAULT URL FOR /routes/user.js]
app.use("/users", userRoutes);

//          [DEFAULT URL FOR /routes/product.js]
app.use("/products", productRoutes);

//          [DEFAULT URL FOR /routes/order.js]
app.use("/orders", orderRoutes);

//          [DEFAULT URL FOR /routes/addToCart.js]
app.use("/addToCard", addToCartRoutes);


//          [SERVER CONNECTION .LISTEN]
app.listen(process.env.PORT || 4000, () => {
    console.log(`Localhost: API is now online on port ${process.env.PORT || 4000}`);
})