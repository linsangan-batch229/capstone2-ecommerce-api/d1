const jwt = require('jsonwebtoken');

//      [TOKEN CREATION]
const secret = "CapstoneAPI";

module.exports.createAccessToken = (user) => {
    const data = {
        id: user._id,
        email : user.user_email,
        isAdmin : user.user_isAdmin
    }

    return jwt.sign(data, secret, {})
}


//      [TOKEN VERIFICATION]

//      [VERIFY USERS]
module.exports.verify = (req, res, next) => {
    
    //token is retrieved from the request header
    let token = req.headers.authorization;
    // console.log(`Your input token : ${token}`);

    //token recived and is not undefine
    if (typeof token !== "undefined") {
        // console.log(`Your input token : ${token}`);

        //separating
        //use slice method
        token = token.slice(7, token.length);

        //validate token
        return jwt.verify(token, secret, (err, data) => {

            //if error
            if (err) {
                return res.send({auth: "failed: invalid token"});
            // if successful
            } else {
                next();
            }
        })
    //token does not exist
    } else {
        return res.send({auth: "failed: token does not exist"})
    };

}

//          [TOKEN DECRYPTION]

module.exports.decode = (token) => {

    //TYPE: BEARER TOKEN
    // if token is not undefine
    if (typeof token !== "undefined"){
        
        //removes the "Bearer " prefix to get the value only
        token = token.slice(7, token.length);

        //verify method
        return jwt.verify(token, secret, (err, data) => {
            if (err) {
                return null;
            } else {
                return jwt.decode(token, {complete: true}).payload;
            }
        })
    } else {
        return null;
    }
}


            
