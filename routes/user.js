const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController.js');
const auth = require("../auth.js");


//          [POST/CREATE - NEW USER REGISTER] 
router.post("/register", (req, res) => {
    userController.registerUser(req.body)
        .then(resultFromController => res.send(resultFromController))
        .catch(error => res.send(error));
  

})

//          [LOGIN TO SHOW AUTH]
router.post("/login", (req, res) => {
    userController.login(req.body)
        .then(resultFromController => res.send(resultFromController))
        .catch(error => res.send(error));
})


//          [RETRIEVE USER DETAILS]
router.get('/userDetails/:userId', (req, res) => {
    userController.getUserDetails(req.params)
        .then(result => res.send(result))       //resolve or success
        .catch(error => res.send(error));       //catch/error if wild card is incorrect
})



//          [RETRIEVE USER DETAILS FROM TOKEN]
router.get("/getDetailsFromToken", auth.verify, (req, res) => {

    const info = auth.decode(req.headers.authorization)

    userController.getProfile({userId: info.id})
        .then(result => res.send(result));
    
})




// //          [PUT/UPDATE A USER AS ADMIN (ADMIN ACCESS ONLY)]
// router.put('/setAdmin/:userId', auth.verify, (req, res) => {
router.put('/setAdmin/:userId', auth.verify, (req, res) => {

    const isAdmin = auth.decode(req.headers.authorization).isAdmin;

    userController.setAdmin(req.params, isAdmin, req.body)
        .then(result => res.send(result))   //resolve or success
        .catch(error => res.send(error));   //catch/error if wild card is incorrect
})




//          [EXPORT 'router' ]
module.exports = router;