//          [IMPORT MODULES]
const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderController.js');
const auth = require("../auth.js");


//          [CREATE ORDER]
router.post('/createOrder', (req, res) => {
    orderController.createOrder(req.body)
        .then(result => res.send(result))   // then/success promise.resolved
        .catch(error => res.send(error));    // catch/error the promise.reject
})


//          [GET ALL ORDERS]
router.get('/getAllOrders', auth.verify, (req, res) => {

    const isAdmin = auth.decode(req.headers.authorization).isAdmin;

    orderController.getAllOrders(isAdmin)
        .then(result => res.send(result))
        .catch(error => res.send(error));
})


//          [GET USER'S ORDERS]
router.get('/getUsersOrder/:userId', (req, res) => {

    orderController.getOrder(req.params)
        .then(result => res.send(result))
        .catch(error => res.send(error));
})





//          [EXPORT 'router' ]
module.exports = router;